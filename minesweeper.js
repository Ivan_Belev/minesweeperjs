document.addEventListener('DOMContentLoaded', () => {
    const grid = document.getElementById('grid');
    const gridSize = { rows: 16, cols: 30 };
    const mineCount = 99;
    let mines = [];
    let cells = [];
    let revealedCount = 0;
    let markedCount = 0;
    let remainingBombsElement = document.getElementById('remaining-bombs');
    let timerElement = document.getElementById('timer');
    let startTime = null;
    let timerInterval = null;

    // Initialize grid and mines
    function init() {
        grid.innerHTML = '';
        mines = [];
        cells = [];
        revealedCount = 0;
        markedCount = 0;
        remainingBombsElement.textContent = `Remaining Bombs: ${mineCount}`;
        timerElement.textContent = 'Time: 0';
        clearInterval(timerInterval);
        startTime = new Date();
        timerInterval = setInterval(updateTimer, 1000);
        grid.style.gridTemplateColumns = `repeat(${gridSize.cols}, 30px)`;
        grid.style.gridTemplateRows = `repeat(${gridSize.rows}, 30px)`;

        for (let i = 0; i < gridSize.rows * gridSize.cols; i++) {
            const cell = document.createElement('div');
            cell.className = 'cell';
            cell.dataset.id = i;
            cell.addEventListener('click', () => revealCell(i));
            cell.addEventListener('contextmenu', (e) => {
                e.preventDefault();
                markCell(i);
            });
            grid.appendChild(cell);
            cells.push(cell);
        }
        generateMines();
    }

    // Generate mines at random positions
    function generateMines() {
        while (mines.length < mineCount) {
            const minePosition = Math.floor(Math.random() * gridSize.rows * gridSize.cols);
            if (!mines.includes(minePosition)) {
                mines.push(minePosition);
            }
        }
    }

    // Reveal a cell
    function revealCell(index) {
        if (cells[index].classList.contains('revealed') || cells[index].classList.contains('marked')) return;
        cells[index].classList.add('revealed');
        revealedCount++;
        if (mines.includes(index)) {
            cells[index].textContent = '💣';
            gameOver(false);
        } else {
            const mineCount = countMinesAround(index);
            if (mineCount > 0) {
                cells[index].textContent = mineCount;
            } else {
                revealAdjacentCells(index);
            }
            checkWin();
        }
    }

    // Mark a cell as a potential bomb
    function markCell(index) {
        if (cells[index].classList.contains('revealed')) return;
        if (cells[index].classList.contains('marked')) {
            cells[index].classList.remove('marked');
            cells[index].textContent = '';
            markedCount--;
        } else {
            cells[index].classList.add('marked');
            cells[index].textContent = '🚩';
            markedCount++;
        }
        remainingBombsElement.textContent = `Remaining Bombs: ${mineCount - markedCount}`;
    }

    // Count mines around a cell
    function countMinesAround(index) {
        const row = Math.floor(index / gridSize.cols);
        const col = index % gridSize.cols;
        let count = 0;
        for (let r = -1; r <= 1; r++) {
            for (let c = -1; c <= 1; c++) {
                const newRow = row + r;
                const newCol = col + c;
                if (newRow >= 0 && newRow < gridSize.rows && newCol >= 0 && newCol < gridSize.cols) {
                    const newIndex = newRow * gridSize.cols + newCol;
                    if (mines.includes(newIndex)) {
                        count++;
                    }
                }
            }
        }
        return count;
    }

    // Reveal adjacent cells if no mines are around
    function revealAdjacentCells(index) {
        const row = Math.floor(index / gridSize.cols);
        const col = index % gridSize.cols;
        for (let r = -1; r <= 1; r++) {
            for (let c = -1; c <= 1; c++) {
                const newRow = row + r;
                const newCol = col + c;
                if (newRow >= 0 && newRow < gridSize.rows && newCol >= 0 && newCol < gridSize.cols) {
                    const newIndex = newRow * gridSize.cols + newCol;
                    if (!cells[newIndex].classList.contains('revealed') && !cells[newIndex].classList.contains('marked')) {
                        revealCell(newIndex);
                    }
                }
            }
        }
    }

    // Check if the player has won the game
    function checkWin() {
        if (revealedCount === gridSize.rows * gridSize.cols - mineCount) {
            gameOver(true);
        }
    }

    // Handle game over
    function gameOver(win) {
        clearInterval(timerInterval);
        setTimeout(() => {
            alert(win ? 'You won the game!' : 'Game Over');
            if (confirm('Do you want to play again?')) {
                init();
            }
        }, 100);
    }

    // Update the timer
    function updateTimer() {
        const elapsedTime = Math.floor((new Date() - startTime) / 1000);
        timerElement.textContent = `Time: ${elapsedTime}`;
    }

    init();
});
